---
title: Database Lab Engine administration
sidebar_label: Overview
slug: /how-to-guides/administration
description: How to administer Database Lab Engine
---

## Guides
- [How to install DLE from the AWS Marketplace](/docs/how-to-guides/administration/install-dle-from-aws-marketplace)
- [How to install Database Lab with Terraform on AWS](/docs/how-to-guides/administration/install-database-lab-with-terraform)
- [How to configure PostgreSQL used by Database Lab Engine](/docs/how-to-guides/administration/postgresql-configuration)
- [How to manage Database Lab Engine](/docs/how-to-guides/administration/engine-manage)
- [How to manage Joe Bot](/docs/how-to-guides/administration/joe-manage)
- [Secure Database Lab Engine](/docs/how-to-guides/administration/engine-secure)
- [Set up machine for Database Lab Engine](/docs/how-to-guides/administration/machine-setup)
- [How to refresh data when working in the "logical" mode](/docs/how-to-guides/administration/logical-full-refresh)
- [Masking sensitive data in PostgreSQL logs when using CI Observer](/docs/how-to-guides/administration/ci-observer-postgres-log-masking)


