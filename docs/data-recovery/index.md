---
title: Overview of data recovery use case
sidebar_label: Data recovery
slug: /data-recovery
---

Recover accidentally deleted data

- In the case of manually deleted data that needs to be restored ASAP, it can be done almost instantly (proper snapshot management configuration is required in Database Lab in advance)
- Using thin cloning, the point-in-time recovery (PITR) can be performed without long waiting
- Typically, for 1 TiB database, PITR requires more than 1 hour in classic setups; with Database Lab, it takes a few seconds to a few minutes

:::note
This page is unfinished. Reach out to the Postgres.ai team to learn more.
:::
