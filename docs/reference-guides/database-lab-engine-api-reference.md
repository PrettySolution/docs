---
title: Database Lab Engine API reference
sidebar_label: Database Lab Engine API
description: "API reference for Database Lab Engine – Swagger"
keywords:
  - "database lab API"
  - "database lab engine API"
  - "postgres cloning API"
---

Please use [Database Lab Swagger](https://postgres.ai/swagger-ui/dblab/) as Database Lab Engine API reference.
