---
id: get-started
title: Database Lab Documentation
hide_title: false
sidebar_label: Documentation Home
slug: /
description: Database Lab is used to boost software development via enabling ultra-fast provisioning of databases of any size. Developers, DBAs, and QA engineers work with full-sized independent clones of PostgreSQL databases. Development and testing tasks are accomplished much faster, with more iterations done, with better quality achieved and with much less money spent.
keywords:
  - "postgres.ai clones"
  - "postgresql clones"
  - "Database Lab Engine"
  - "Dev/QA/Staging databases with superpowers"
  - "postgres-checkup"
  - "Joe bot for SQL optimization"
  - "SQL optinization on clones"
  - "thin clones for Postgres"
  - "database testing in CI/CD"
  - "postgres-checkup"
---

<table class='docs-home'>
  <tr>
    <td>
      <h3>Getting started</h3>
      <ul>
        <li><a href='/docs/how-to-guides/administration/install-database-lab-with-terraform'>Install Database Lab with Terraform on AWS</a></li>
        <li><a href='/docs/tutorials/database-lab-tutorial-amazon-rds'>Engine setup for Amazon RDS</a></li>
        <li><a href='/docs/tutorials/database-lab-tutorial'>Engine setup for any PostgreSQL database</a></li>
        <li><a href='/docs/tutorials/joe-setup'>Joe Bot setup</a></li>
        <li><a href='/docs/questions-and-answers'>FAQ</a></li>
      </ul>
    </td>
    <td>
      <h3>Product & use cases</h3>
      <ul>
        <li><a href='/products/how-it-works'>How it works</a></li>
        <li><a href='/products/database-migration-testing'>Database migration testing</a></li>
        <li><a href='/products/joe'>SQL optimization</a></li>
        <li><a href='/products/realistic-test-environments'>Realistic dev & test environments</a></li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>
      <h3>Security</h3>
      <ul>
        <li><a href='/docs/platform/security'>Platform security</a></li>
        <li><a href='/docs/database-lab/masking'>Data masking setup & configuration</a></li>
        <li><a href='/docs/how-to-guides/administration/engine-secure'>How to make work with Database Lab secure</a></li>
      </ul>
    </td>
    <td>
      <h3>Reference guides for users</h3>
      <ul>
        <li><a href='/docs/reference-guides/database-lab-engine-components'>Database Lab Engine (DLE) components</a></li>
        <li><a href='/docs/reference-guides/dblab-client-cli-reference'>DLE client CLI</a></li>
        <li><a href='/docs/reference-guides/joe-bot-commands-reference'>Joe Bot commands</a></li>
      </ul>
    </td>
  </tr>
</table>
