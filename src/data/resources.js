const resources = [
  {
    title: 'GitLab: How GitLab iterates on SQL performance optimization workflow to reduce downtime risks',
    description: 'Learn how SaaS company can improve database management processes.',
    preview: '/assets/thumbnails/case-study-gitlab.png',
    link: '/resources/case-studies/gitlab',
    internalLink: true,
    kind: 'Case study',
    date: '2021-06-18 20:00:00',
  },
  {
    title: 'Qiwi: Control the Data with Database Lab to Accelerate Development',
    description: 'Learn how banks can overcome test data management pitfalls to eliminate production downtime related to data operations.',
    preview: '/assets/thumbnails/case-study-qiwi.png',
    link: '/resources/case-studies/qiwi',
    internalLink: true,
    kind: 'Case study',
    date: '2020-11-11 15:00:00',
  },
  {
    title: 'CDEK: Speed up Product Development with Database Lab',
    description: 'Learn how Database Lab speeding up development in a logistics company.',
    preview: '/assets/thumbnails/case-study-cdek.png',
    link: '/resources/case-studies/cdek',
    internalLink: true,
    kind: 'Case study',
    date: '2020-11-03 15:00:00',
  },
  {
    title: 'Tutorial: Database Lab Engine',
    description: '',
    preview: '/assets/thumbnails/katakoda-tutorial-dle.png',
    link: 'https://www.katacoda.com/postgres-ai/scenarios/database-lab-tutorial',
    kind: 'Tutorial',
    date: '2020-11-01 15:00:00',
  },
  {
    title: 'How to Prepare PostgreSQL Databases for Black Friday',
    description: 'Learn about best practices in preparing PostgreSQL databases for peak seasons in e-commerce companies.',
    preview: '/assets/thumbnails/wp1.png',
    link: '/get-whitepaper-how-to-prepare-postgreqsl-databases-for-black-friday-and-cyber-monday',
    kind: 'White paper',
    date: '2020-11-05 15:00:00',
  },
];

export default resources;
