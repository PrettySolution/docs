const careers = [
  {
    title: 'Senior Database Engineer | Postgres',
    descriptions: [
      '3+ years of experience running PostgreSQL in large production environments',
      'Strong skills in performance optimization of large and heavily loaded databases (>1TiB, >10k TPS)',
      'Readiness to dive deep into PostgreSQL internals',
    ],
    link: '/careers/dba',
  },
  {
    title: 'Senior Full Stack Developer | React | Go',
    descriptions: [
      '3+ years experience in client-side development using React',
      'Strong HTML, CSS, JavaScript skills',
      'Readiness to work with backend code',
    ],
    link: '/careers/fullstack',
  },
  {
    title: 'Senior Software Engineer | Go',
    descriptions: [
      '3+ years experience developing server applications using Go',
      'Deep understanding of HTTP protocol, data structures, JSON',
      'REST API development experience',
    ],
    link: '/careers/godeveloper',
  },
  {
    title: 'Senior Frontend Engineer | React',
    descriptions: [
      '3+ years experience in client-side development using React',
      'Excellent HTML, CSS, JavaScript skills – you understand not only how to build the data, but how to make it look great too',
      'Strong experience in all aspects of client-side performance optimization',
    ],
    link: '/careers/frontend',
  },
];

export default careers;
