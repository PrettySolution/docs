# Principles
This repository contains both Postgres.ai website (excluding the Console/Platform part) and Postgres.ai Documentation.

## Documentation – principles

We're building documentation following the principles described at https://documentation.divio.com/:

> There is a secret that needs to be understood in order to write good software documentation: there isn’t one thing called documentation, there are four.
>
> They are: tutorials, how-to guides, technical reference and explanation. They represent four different purposes or functions, and require four different approaches to their creation. Understanding the implications of this will help improve most documentation - often immensely.

Learn more: https://documentation.divio.com/

## Engine

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

### Installation / Build

```shell
$ npm install
```

### Local Development

```shell
$ npm run start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.
